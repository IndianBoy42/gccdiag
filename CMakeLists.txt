cmake_minimum_required(VERSION 3.19)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

#
# Set up CMake local modules
#
list(
  APPEND
  CMAKE_MODULE_PATH
  "${CMAKE_SOURCE_DIR}/cmake"
)

#
# Project details
#

project(
  "gccdiag"
  LANGUAGES CXX
)

#
# Default build type
#
set(default_build_type "Release")
if(EXISTS "${CMAKE_SOURCE_DIR}/.git")
  set(default_build_type "Debug")
endif()

if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(STATUS "Setting build type to '${default_build_type}' as none was specified.")
  set(
    CMAKE_BUILD_TYPE "${default_build_type}" CACHE
    STRING "Choose the type of build." FORCE
  )
  # Set the possible values of build type for cmake-gui
  set_property(
    CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS
    "Debug" "Release" "MinSizeRel" "RelWithDebInfo"
  )
endif()

#
# Import options that distinguish packaged source builds and git checkouts
#
option(${PROJECT_NAME}_SOURCE_DIST OFF)
if(EXISTS "${CMAKE_SOURCE_DIR}/cmake/SourceDist.cmake")
  include(SourceDist)
endif()

if(${PROJECT_NAME}_SOURCE_DIST)
  message(
    "Building from a source dist: header-only deps are included, Conan not "
    "used for downloading packages, version read from file."
  )
  if(NOT DEFINED ${PROJECT_NAME}_USE_CONAN)
    set(${PROJECT_NAME}_USE_CONAN OFF)
  endif()
else()
  message(
    "Building from a git checkout: Conan used for deps, version obtained from "
    "git."
  )
  if(NOT DEFINED ${PROJECT_NAME}_USE_CONAN)
    set(${PROJECT_NAME}_USE_CONAN ON)
  endif()
endif()


#
# Set project options
#

include(StandardSettings)
include(Utils)
message(STATUS "Started CMake for ${PROJECT_NAME}...\n")

#
# Setup alternative names
#

if(${PROJECT_NAME}_USE_ALT_NAMES)
  string(TOLOWER ${PROJECT_NAME} PROJECT_NAME_LOWERCASE)
  string(TOUPPER ${PROJECT_NAME} PROJECT_NAME_UPPERCASE)
else()
  set(PROJECT_NAME_LOWERCASE ${PROJECT_NAME})
  set(PROJECT_NAME_UPPERCASE ${PROJECT_NAME})
endif()

#
# Get CPM (CMake Package Manager)
#
include(cmake/CPM.cmake)

# Add clang-format CMake script
cpmaddpackage(
  NAME Format.cmake
  VERSION 1.7.2
  GITHUB_REPOSITORY TheLartians/Format.cmake
  OPTIONS # set to yes skip cmake formatting
  "FORMAT_SKIP_CMAKE NO"
  # path to exclude (optional, supports regular expressions)
  "CMAKE_FORMAT_EXCLUDE cmake/CPM.cmake"
)

#
# Version derivation based on git tag
#
if(NOT ${PROJECT_NAME}_SOURCE_DIST)
  include(VersionFromGit)
  version_from_git(
    INCLUDE_HASH ON
    MARK_TREE_DIRTY ON
    LOG ON
    TIMESTAMP "%Y%m%dT%H%M%SZ" UTC
  )
endif()
set(PROJECT_VERSION VERSION)

#
# Prevent building in the source directory
#

if(PROJECT_SOURCE_DIR STREQUAL PROJECT_BINARY_DIR)
  message(FATAL_ERROR "In-source builds not allowed. Please make a new directory (called a build directory) and run CMake from there.\n")
endif()

#
# Create library, setup header and source files
#

# Find all headers and implementation files
include(SourcesAndHeaders)
add_executable(${PROJECT_NAME} ${exe_sources})

verbose_message("Found the following source files:")
if(${PROJECT_NAME}_BUILD_EXECUTABLE)
  verbose_message(${exe_sources})
else()
  verbose_message(${sources})
endif()
message(STATUS "Added all header and implementation files.\n")

#
# Set the project standard and warnings
#

if(${PROJECT_NAME}_BUILD_HEADERS_ONLY)
  target_compile_features(${PROJECT_NAME} INTERFACE cxx_std_20)
else()
  target_compile_features(${PROJECT_NAME} PUBLIC cxx_std_20)
endif()
include(CompilerWarnings)
set_project_warnings(${PROJECT_NAME})

verbose_message("Applied compiler warnings. Using standard ${CXX_STANDARD}.\n")

#
# Set the build/user include directories
#

# Allow usage of header files in the `src` directory, but only for utilities
if(${PROJECT_NAME}_BUILD_HEADERS_ONLY)
  target_include_directories(
    ${PROJECT_NAME}
    INTERFACE
    $<INSTALL_INTERFACE:include>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  )
else()
  target_include_directories(
    ${PROJECT_NAME}
    PUBLIC
    $<INSTALL_INTERFACE:include>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/include>
    PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/src
  )
endif()

message(STATUS "Finished setting up include directories.")

#
# Model project dependencies
#
include(CMakeDependentOption)
cmake_dependent_option(
  ${PROJECT_NAME}_USE_CONAN_BOOST "Use Conan version of Boost"
  ON
  "${PROJECT_NAME}_USE_CONAN"
  OFF
)
if(${PROJECT_NAME}_USE_CONAN)
  message(STATUS "Using Conan to obtain libs")
  set(CONAN_CMAKE_CXX_STANDARD ${CMAKE_CXX_STANDARD})
  include(Conan)


  if(${PROJECT_NAME}_USE_CONAN_BOOST)
    conan_cmake_configure(
      REQUIRES
      taywee-args/6.2.7
      boost/1.76.0
      GENERATORS cmake_find_package
    )
  else()
    conan_cmake_configure(
      REQUIRES
      taywee-args/6.2.7
      GENERATORS cmake_find_package
    )
  endif()

  conan_cmake_autodetect(settings)
  conan_cmake_install(
    PATH_OR_REFERENCE .
    BUILD missing
    REMOTE conancenter
    SETTINGS ${settings}
  )
else()
  list(
    APPEND
    CMAKE_MODULE_PATH
    "${CMAKE_SOURCE_DIR}/cmake/packages"
  )
  message(STATUS "Using system libs.")
endif()

find_package(Boost 1.74...1.76 COMPONENTS headers filesystem json regex REQUIRED)
target_link_libraries(
  ${PROJECT_NAME}
  PRIVATE
  Boost::headers
  Boost::filesystem
  Boost::json
  Boost::regex
)
target_link_libraries(
  ${PROJECT_NAME}
  INTERFACE
  Boost::container_hash
)
target_include_directories(
  ${PROJECT_NAME}
  PRIVATE
  ${Boost_json_INCLUDE_DIRS}
)

if(${PROJECT_NAME}_USE_CONAN)
  find_package(args 6.2.7 REQUIRED)
  target_include_directories(
    ${PROJECT_NAME}
    PRIVATE
    ${taywee_args_INCLUDE_DIRS}
  )
else()
  find_package(args REQUIRED)
  target_include_directories(
    ${PROJECT_NAME}
    PRIVATE
    ${args_INCLUDE_DIRS}
  )
endif()

verbose_message("Successfully added all dependencies and linked against them.")

#
# Provide alias to library for
#

if(${PROJECT_NAME}_BUILD_EXECUTABLE)
  add_executable(${PROJECT_NAME}::${PROJECT_NAME} ALIAS ${PROJECT_NAME})
else()
  add_library(${PROJECT_NAME}::${PROJECT_NAME} ALIAS ${PROJECT_NAME})
endif()

verbose_message("Project is now aliased as ${PROJECT_NAME}::${PROJECT_NAME}.\n")

#
# Install library for easy downstream inclusion
#

include(GNUInstallDirs)
install(
  TARGETS
  ${PROJECT_NAME}
  EXPORT
  ${PROJECT_NAME}Targets
  LIBRARY DESTINATION
  ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION
  ${CMAKE_INSTALL_BINDIR}
  ARCHIVE DESTINATION
  ${CMAKE_INSTALL_LIBDIR}
  INCLUDES DESTINATION
  include
  PUBLIC_HEADER DESTINATION
  include
)

install(
  EXPORT
  ${PROJECT_NAME}Targets
  FILE
  ${PROJECT_NAME}Targets.cmake
  NAMESPACE
  ${PROJECT_NAME}::
  DESTINATION
  ${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}
)

#
# Add version header
#

configure_file(
  ${CMAKE_CURRENT_LIST_DIR}/cmake/version.hpp.in
  include/${PROJECT_NAME_LOWERCASE}/version.hpp
  @ONLY
)

install(
  FILES
  ${CMAKE_CURRENT_BINARY_DIR}/include/${PROJECT_NAME_LOWERCASE}/version.hpp
  DESTINATION
  include/${PROJECT_NAME_LOWERCASE}
)

#
# Install the `include` directory
#

install(
  DIRECTORY
  include/${PROJECT_NAME_LOWERCASE}
  DESTINATION
  include
)

verbose_message("Install targets succesfully build. Install with `cmake --build <build_directory> --target install --config <build_config>`.")

#
# Quick `ConfigVersion.cmake` creation
#

include(CMakePackageConfigHelpers)
write_basic_package_version_file(
  ${PROJECT_NAME}ConfigVersion.cmake
  VERSION
  ${VERSION}
  COMPATIBILITY
  SameMajorVersion
)

configure_package_config_file(
  ${CMAKE_CURRENT_LIST_DIR}/cmake/${PROJECT_NAME}Config.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Config.cmake
  INSTALL_DESTINATION
  ${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}
)

install(
  FILES
  ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Config.cmake
  ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake
  DESTINATION
  ${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}
)

#
# Generate export header if specified
#

if(${PROJECT_NAME}_GENERATE_EXPORT_HEADER)
  include(GenerateExportHeader)
  generate_export_header(${PROJECT_NAME})
  install(
    FILES
    ${PROJECT_BINARY_DIR}/${PROJECT_NAME_LOWERCASE}_export.h
    DESTINATION
    include
  )
  message(STATUS "Generated the export header `${PROJECT_NAME_LOWERCASE}_export.h` and installed it.")
endif()

message(STATUS "Finished building requirements for installing the package.\n")

#
# Generate a source distribution
#

include(CPackConf)
