#pragma once

#include <filesystem>
#include <functional>
#include <map>
#include <utility>

#include <boost/json.hpp>

namespace fs = std::filesystem;
using fs::path;
using std::string;
using std::vector;

namespace std {
template <>
struct hash<path> {
    size_t operator()(const path &p) const
    {
        return std::filesystem::hash_value(p);
    }
};
} // namespace std

namespace gccdiag {
struct CompilationDatabaseEntry {
    path build_dir;
    vector<string> cmd;
};

std::ostream &operator<<(std::ostream &os, const CompilationDatabaseEntry &c);

using CompilationDatabase = std::unordered_map<path, CompilationDatabaseEntry>;

CompilationDatabase json_file_to_compilation_db(const path &file_path);
std::ostream &operator<<(std::ostream &os, const CompilationDatabase &c);
} // namespace gccdiag
