#pragma once
#include "gccdiag/iterator_facade.hpp"

#include <string>

enum class month : int {
    january,
    february,
    march,
    april,
    may,
    june,
    july,
    august,
    september,
    october,
    november,
    december,
};

constexpr std::array month_names = {
    "january", "february", "march",     "april",   "may",      "june",
    "july",    "august",   "september", "october", "november", "december",
};

inline std::string to_string(month m) { return month_names.at(size_t(m)); }

class month_iterator : public util::iterator::iterator_facade<month_iterator> {
    month _cur = month::january;

public:
    month_iterator() = default;
    explicit month_iterator(month m) : _cur(m) { }

    auto begin() const { return *this; }
    auto end() const
    {
        return month_iterator(month(int(month::december) + 1));
    }

    void advance(int off) { _cur = month(int(_cur) + off); }

    const month &dereference() const { return _cur; }

    bool equal_to(month_iterator other) const { return _cur == other._cur; }

    int distance_to(month_iterator other) const
    {
        return int(other._cur) - int(_cur);
    }
};

struct month_iterator_range {
    auto begin() const { return month_iterator {}; }
    auto end() const { return month_iterator().end(); }
};
