stages:
  - build
  - upload
  - release

variables:
  # Package version can only contain numbers (0-9), and dots (.).
  # Must be in the format of X.Y.Z, i.e. should match /\A\d+\.\d+\.\d+\z/ regular expresion.
  # See https://docs.gitlab.com/ee/user/packages/generic_packages/#publish-a-package-file
  SRC_NO_VER: "gccdiag"
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}"

cache:
  - key: fixed
    paths:
      - /root/.conan

build:
  stage: build

  image: gcc:bullseye
  before_script:
    - export DEBIAN_FRONTEND=noninteractive
    - >-
      echo 'deb http://deb.debian.org/debian bullseye-backports main' >
      /etc/apt/sources.list.d/backports.list
    - apt-get update --yes
    - apt-get install --yes --no-install-recommends cmake/bullseye-backports git python3 python3-pip
    - pip3 install conan

  script:
    - cmake -S. -B build
    - cpack --config build/CPackSourceConfig.cmake
    - cmake --build build

  artifacts:
    paths:
      - build/package/

upload:
  stage: upload
  image: curlimages/curl:latest
  script:
    - >
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}"
      --upload-file build/package/${SRC_NO_VER}-${CI_COMMIT_TAG}.tar.gz
      ${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/${SRC_NO_VER}-${CI_COMMIT_TAG}.tar.gz
  only:
    - tags

release:
  # Caution, as of 2021-02-02 these assets links require a login, see:
  # https://gitlab.com/gitlab-org/gitlab/-/issues/299384
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - >
      echo 'The archive listed as "other" is the proper source release, in
      which the version is hard-coded (it is otherwise inferred from git tag).
      Please use this archive when creating Linux packages, or build from a git
      checkout.' > description_file
    - >
      release-cli create
      --name "Release $CI_COMMIT_TAG"
      --tag-name $CI_COMMIT_TAG
      --description description_file
      --assets-link "{
      \"name\":\"${SRC_NO_VER}-${CI_COMMIT_TAG}.tar.gz\",
      \"type\":\"Ready-to-build source code\",
      \"url\":\"${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/${SRC_NO_VER}-${CI_COMMIT_TAG}.tar.gz\"
      }"
  only:
    - tags
