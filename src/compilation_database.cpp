#include "gccdiag/compilation_database.hpp"

#include "gccdiag/json_utils.hpp"
#include "gccdiag/vector_print.hpp"

#include <iostream>
#include <string>
#include <vector>

using std::string;
using std::vector;

namespace gccdiag {
CompilationDatabase json_file_to_compilation_db(const path &file_path)
{
    try {
        auto jv = json_value_from_file(file_path);
        auto arr = jv.get_array();
        CompilationDatabase cd {};
        for (const auto &el : arr) {
            const auto &obj = el.as_object();
            auto src_path = get_path_from_json_throw(obj, "file");
            auto build_dir = get_path_from_json_throw(obj, "directory");
            auto cmd =
                get_string_vector_from_json_string_throw(obj, "command");
            cd[src_path] = CompilationDatabaseEntry { build_dir, cmd };
        }
        return cd;
    } catch (...) {
        std::cerr
            << "An error occured while parsing the compilation database:\n";
        throw;
    }
}

std::ostream &operator<<(std::ostream &os, const CompilationDatabaseEntry &c)
{
    using namespace gccdiag::util;
    os << "{ build_dir: " << c.build_dir << ", cmd: " << c.cmd << " }";

    return os;
}

std::ostream &operator<<(std::ostream &os, const CompilationDatabase &c)
{
    using namespace gccdiag::util;
    for (auto const &pair : c) {
        os << "{ " << pair.first << ": " << pair.second.cmd << " }\n";
    }

    return os;
}

} // namespace gccdiag
