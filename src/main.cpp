#include "gccdiag/compilation_database.hpp"
#include "gccdiag/config.hpp"
#include "gccdiag/config_dir.hpp"
#include "gccdiag/exceptions.hpp"
#include "gccdiag/parent_path_iterator.hpp"
#include "gccdiag/split_string.hpp"
#include "gccdiag/vector_print.hpp"
#include "gccdiag/version.hpp"

#include <algorithm>
#include <array>
#include <filesystem>
#include <optional>
#include <ranges>
#include <sstream>
#include <stdexcept>
#include <utility>

#include <args.hxx>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/process.hpp>
#include <boost/process/exception.hpp>
#include <boost/process/search_path.hpp>
#include <boost/range.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/combine.hpp>
#include <boost/range/join.hpp>

using std::cerr;
using std::cout;
using std::optional;
using std::string;
using std::vector;
using namespace std::string_literals;
namespace fs = std::filesystem;
namespace bp = boost::process;
using fs::path;
using gccdiag::CompilationDatabase;
using gccdiag::Config;

path find_in_dir_or_above(const path &dir, const string &filename)
{
    util::iterator::path_ancestor_iterator it(dir);
    auto result =
        *std::find_if(it, std::end(it), [&filename](const auto &curr_dir) {
            return fs::is_regular_file(curr_dir / filename);
        });

    if (!result.empty()) {
        return result /= path(filename);
    }
    return result;
}

path find_config_file(const fs::path &current_dir)
{
    auto parent_dirs = util::iterator::path_ancestor_iterator { current_dir };
    std::vector config_dir { get_config_dir("gccdiag") };
    auto r1 =
        boost::make_iterator_range(parent_dirs.begin(), parent_dirs.end());
    auto r2 = boost::make_iterator_range(config_dir);
    auto candidate_dirs = boost::join(r1, r2);

    path return_val;
    auto res = boost::find_if(candidate_dirs, [&return_val](const auto &dir) {
        if (auto p = dir / ".gccdiag"; fs::is_regular_file(p)) {
            return_val = p;
            return true;
        }
        if (auto p = dir / "_gccdiag"; fs::is_regular_file(p)) {
            return_val = p;
            return true;
        }
        return false;
    });
    return return_val;
}

path find_compilation_db(const fs::path &current_dir)
{
    auto parent_dirs = util::iterator::path_ancestor_range { current_dir };

    path return_val;
    auto res = std::find_if(parent_dirs.begin(), parent_dirs.end(),
                            [&return_val](const auto &dir) {
                                if (auto p = dir / "compile_commands.json";
                                    fs::is_regular_file(p)) {
                                    return_val = p;
                                    return true;
                                }
                                return false;
                            });
    return return_val;
}

struct ArgsSplitter {
    void operator()(const std::string & /*name*/, const std::string &value,
                    vector<string> &destination)
    {
        destination = gccdiag::util::split_string(value);
    };
};

path boost_path_to_std_path(const boost::filesystem::path &p)
{
    return path(p.string());
}

path find_executable_path(const path &raw)
{
    auto bpath = boost::filesystem::path(raw.string());
    if (path path_from_env; raw.parent_path().empty()
        && !(path_from_env =
                 boost_path_to_std_path(boost::process::search_path(bpath)))
                .empty()) {
        return path_from_env;
    }
    return raw;
}

namespace args {
template <typename Option>
auto get_optional(Option &option_)
    -> optional<std::remove_cvref_t<decltype(option_.Get())>>
{
    if (option_) {
        return args::get(option_);
    }
    return {};
}
} // namespace args

vector<string> find_compilation_command(const path &src_path,
                                        CompilationDatabase &comp_db,
                                        const Config &config)
{
    auto ext = src_path.extension();
    // source files are found in the compilation db (or not, in which case we
    // default to non-db result)
    if (".cpp" == ext || ".c" == ext || ".cxx" == ext || ".cc" == ext) {
        auto it = comp_db.find(src_path);
        if (it != comp_db.end()) {
            auto res = it->second.cmd;
            return { res.begin(), res.end() };
        }
        vector non_db_res = *config.default_args();
        non_db_res.push_back(std::string(src_path));
        return non_db_res;
    }
    // with header files, we either find the matching c[pp] file or return the
    // default, non-db result
    if (".hpp" == ext || ".h" == ext || ".hxx" == ext) {
        constexpr std::array candidate_exts = { ".cpp", ".c", ".cxx" };
        // option 1: find a c/cpp/cxx file with same path in compilation
        // database
        for (const auto &e : candidate_exts) {
            auto tp = src_path;
            tp.replace_extension(e);
            auto it = comp_db.find(tp);
            if (it != comp_db.end()) {
                auto res = it->second.cmd;
                return { std::next(res.begin()), res.end() };
            }
        }
        // option 2: find a c/cpp/cxx file with same name, any path in
        // compilation database
        for (const auto &e : candidate_exts) {
            auto filename = src_path.filename();
            filename.replace_extension(e);
            auto it = boost::find_if(comp_db, [&filename](const auto &pr) {
                const auto &[p, _] = pr;
                return p.filename() == filename;
            });
            if (it != comp_db.end()) {
                auto res = it->second.cmd;
                return { std::next(res.begin()), res.end() };
            }
        }
        // option 3: return default args
        vector<string> non_db_res = *config.default_args();
        non_db_res.push_back(std::string(src_path));
        return non_db_res;
    }
    throw std::runtime_error("Unknown file type.");
}

int main(int argc, char **argv)
{
    args::ArgumentParser parser(
        "Run `gcc` (or other compiler) on a source file to obtain "s
        "diagnostics, "s
        "with compile flags from `compile_commands.json`."s,
        "The program searches the current folder folders above it in the "
        "hierarchy for a \"compile_commands.json\" file, until it finds the "
        "first match. This file is then parsed as the compilation "
        "database.\n\n\n"
        "The program can also be configured by reading a \".gccdiag\" or "
        "\"_gccdiag\" file in the current working directory or above, as well "
        "as the global one in $XDG_CONFIG_HOME/gccdiag/ "
        "This file contains a JSON dict with up to three keys: "
        "\"additional_args\", \"default_args\", \"args_to_ignore\", which "
        "correspond to flags listed above.");
    args::Positional<std::string> src(parser, "source", "Path to file",
                                      std::string(), args::Options::Required);
    args::ValueFlag<vector<string>, ArgsSplitter> args_to_add(
        parser, "args", "Compiler args to add when calling (empty by default)",
        { 'a', "add-args" });
    args::ValueFlag<path> compiler(
        parser, "path",
        "Compiler path (empty by default). If empty, the compiler will be "
        "inferred from the compilation database",
        { 'c', "compiler" }, "gcc");
    args::ValueFlag<vector<string>, ArgsSplitter> default_args(
        parser, "args",
        "Default args to use if the file isn't found in "
        "`compile_commands.json`; if empty, the compiler isn't invoked (empty "
        "by default; \"-S -x c\" or \"-S -x c++\" are reasonable "
        "alternatives, but will yield some false positives)",
        { 'd', "default-args" });
    args::ValueFlag<vector<string>, ArgsSplitter> args_to_ignore(
        parser, "args", "Compiler args to omit when calling",
        { 'i', "ignore-args" });
    args::HelpFlag help(parser, "help", "Prints help information",
                        { 'h', "help" });
    args::HelpFlag version(parser, "version",
                           "Prints the version number and exits",
                           { 'v', "version" });
    try {
        parser.ParseCLI(argc, argv);

        // obtain the configuration, in order of rising significance:
        // - from defaults
        // - from a config file (.gccdiag or _gccdiag)
        // - from command line as args
        Config config = Config::defaults();

        auto config_file_path = find_config_file(fs::current_path());

        if (!config_file_path.empty()) {
            config.update(gccdiag::config_from_file(config_file_path));
        }

        Config args_config = {
            args::get_optional(compiler),
            args::get_optional(args_to_add),
            args::get_optional(default_args),
            args::get_optional(args_to_ignore),
        };

        config.update(args_config);

        // obtain desired file path
        auto src_path = fs::absolute(args::get(src));

        // parse compilation database
        auto cd_path = find_compilation_db(fs::current_path());
        if (cd_path.empty()) {
            throw(
                gccdiag::path_error("Unable to locate `compile_commands.json` "
                                    "in current working directory or above."));
        }
        auto cd = gccdiag::json_file_to_compilation_db(cd_path);

        // if the compiler_path is not empty, use its contents to replace the
        // compiler in compile command
        bool replace_compiler = !config.compiler_path()->empty();

        vector<string> gcc_args {};

        auto ccmd = find_compilation_command(src_path, cd, config);
        // by default, the whole ccmd string is used
        auto ccmd_begin = ccmd.cbegin();

        if (replace_compiler) {
            // start with compiler path
            gcc_args.push_back(
                string(find_executable_path(*config.compiler_path())));
            // move the ccmd begin iterator one element forward as to omit the
            // first element (compiler)
            ccmd_begin = std::next(ccmd_begin);
        }

        // add args from compilation_database, if they are not among exceptions
        std::copy_if(
            ccmd_begin, ccmd.cend(), std::back_inserter(gcc_args),
            [&exc = std::as_const(*config.args_to_ignore())](const auto &a) {
                return exc.end() == boost::find(exc, a);
            });
        // add additional_args
        std::copy(config.additional_args()->begin(),
                  config.additional_args()->end(),
                  std::back_inserter(gcc_args));
        // replace output arg with /dev/null
        if (auto dash_oh_it =
                std::find(gcc_args.begin(), gcc_args.end(), "-o");
            dash_oh_it != gcc_args.end()) {
            auto out_it = std::next(dash_oh_it);
            if (out_it != gcc_args.end()) {
                *out_it = "/dev/null";
            }
        }

        // call the program
        try {
            auto res = bp::system(gcc_args, bp::std_out > stdout,
                                  bp::std_err > stderr, bp::std_in < stdin);
            return res;
        } catch (const boost::process::process_error &e) {
            std::ostringstream buffer;
            using namespace gccdiag::util;
            buffer << "Error occurred while trying to run command "
                   << gcc_args;
            throw boost::process::process_error(e.code(), buffer.str());
        }
    } catch (const args::Completion &e) {
        cout << e.what();
        return 0;
    } catch (const args::Help &e) {
        if (*(e.what()) == 'h') {
            cout << parser;
        } else {
            cout << "gccdiag v" << GCCDIAG_VERSION << "\n";
        }
        return 0;
    } catch (const args::ParseError &e) {
        cerr << e.what() << std::endl;
        cerr << parser;
        cerr << "Consult --help for usage.\n";
        return 1;
    } catch (std::exception &err) {
        cerr << err.what() << "\n";
        return 1;
    }
}
